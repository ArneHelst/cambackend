/*
   For step-by-step instructions on connecting your Android application to this backend module,
   see "App Engine Java Endpoints Module" template documentation at
   https://github.com/GoogleCloudPlatform/gradle-appengine-templates/tree/master/HelloEndpoints
*/

package com.example.Anders.myapplication.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.google.api.server.spi.config.Named;

/** An endpoint class we are exposing */
@Api(
  name = "myApi",
  version = "v1",
  namespace = @ApiNamespace(
    ownerDomain = "backend.myapplication.Anders.example.com",
    ownerName = "backend.myapplication.Anders.example.com",
    packagePath=""
  )
)
public class MyEndpoint {
    /** A simple endpoint method that takes a name and says Hi back */
    @ApiMethod(name = "sayHi")
    public MyBean sayHi(@Named("name") String name) {
        MyBean response = new MyBean();
        //response.setData("Hi, " + name);
        response.setData("http://root:root@192.168.0.196/axis-cgi/mjpg/video.cgi");
        return response;
    }

    @ApiMethod(name="Stream",
            httpMethod = ApiMethod.HttpMethod.POST)
         public MyBean stream(@Named("pass") String pass) {
        MyBean response = new MyBean();
        if(pass.contentEquals("77")){
            response.setData("http://root:root@192.168.0.196/axis-cgi/mjpg/video.cgi");
        }else {
            response.setData("error");
        }
        return response;
    }

    @ApiMethod(name="Auth",
            httpMethod = ApiMethod.HttpMethod.POST)
    public MyBean Auth(@Named("pass") String pass) {
        MyBean response = new MyBean();
        if(pass.contentEquals("77")){
            response.setData("ok");
        }else {
            response.setData("fail");
        }
        return response;
    }

}
