package com.example.Anders.myapplication.backend;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiNamespace;

import java.util.logging.Logger;

import javax.inject.Named;

/**
 * An endpoint class we are exposing
 */
@Api(
        name = "cameraAccessApi",
        version = "v1",
        resource = "cameraAccess",
        namespace = @ApiNamespace(
                ownerDomain = "backend.myapplication.Anders.example.com",
                ownerName = "backend.myapplication.Anders.example.com",
                packagePath = ""
        )
)

public class CameraAccessEndpoint {

    private static final Logger logger = Logger.getLogger(CameraAccessEndpoint.class.getName());

    /**
     * This method gets the <code>CameraAccess</code> object associated with the specified <code>id</code>.
     *
     * @param id The id of the object to be returned.
     * @return The <code>CameraAccess</code> associated with <code>id</code>.
     */
    @ApiMethod(name = "getCameraAccess")
    public CameraAccess getCameraAccess(@Named("id") Long id) {
        // TODO: Implement this function
        logger.info("Calling getCameraAccess method");
        CameraAccess ca = new CameraAccess();
        //ca.url = "http://root:root@192.168.0.196/axis-cgi/mjpg/video.cgi";
        ca.url = "Hejsan";
        return ca;
    }

    /**
     * This inserts a new <code>CameraAccess</code> object.
     *
     * @param cameraAccess The object to be added.
     * @return The object to be added.
     */
    @ApiMethod(name = "insertCameraAccess")
    public CameraAccess insertCameraAccess(CameraAccess cameraAccess) {
        // TODO: Implement this function
        CameraAccess ca = new CameraAccess();
        //ca.url = "http://root:root@192.168.0.196/axis-cgi/mjpg/video.cgi";
        ca.url = "Hejsan";
        return ca;
    }
}