package m.smarthomev2.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class HttpMet {

    private final String USER_AGENT = "Mozilla/5.0";
    String user;
    String url;
    String pass;
    public String authenticate(String user, String pass, String url) throws IOException, JSONException {

        this.url =url;
        this.user = user;
        this.pass = pass;
        java.net.URL obj = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
        //add request header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        con.setRequestProperty("Content-Type","application/json");
        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        JSONObject params = new JSONObject();
        try {
            params.put("pass",pass);
            params.put("user",user);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        wr.writeBytes(params.toString());
        wr.flush();
        wr.close();
        int responseCode = con.getResponseCode();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String ts = Parser.amazonParse(response.toString());
        System.out.println("ts: " + ts);
        return ts;
    }


}

