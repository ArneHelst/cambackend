package m.smarthomev2.utils;

import java.math.BigInteger;

public class Encrypt {
    int key;

    public Encrypt(int key){
        this.key = key;
    }

    public Long encode(String decoded){
        String tempVal = null;
        for(int i=0;i<decoded.length();i++){
            tempVal += decoded.charAt(i);
        }
        System.out.println("tempVal: " + tempVal );


        int decodedASCI = Integer.valueOf(tempVal);
        System.out.println("ASCI OF: ->" + decodedASCI + "<-");
        //Power to key
        BigInteger bi = BigInteger.valueOf(decodedASCI);
        return Long.getLong(bi.pow(key).toString());
    }

    public void decode(String encoded){

    }

    public static void main(String[] args) throws Exception {
        System.out.println("START");
        Encrypt en = new Encrypt(77);
        System.out.println("Key: ->" + en.encode("hej") + "<-");
    }

}
