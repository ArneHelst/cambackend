package m.smarthomev2.utils;


import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Anders on 2016-02-29.
 */
public class OKHttp {
    String user;
    String url;
    String pass;

    public String authenticate(String user, String pass, String url){

        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        JSONObject json= new JSONObject();
        String strReturn = "";
        try {
            json.put("pass",pass);
            json.put("user", user);
        } catch (JSONException e) {
            System.out.println("Error JSON");
            e.printStackTrace();
        }


        //OkHttpClient c = new OkHttpClient().
        OkHttpClient client = InsecureClient.getUnsafeOkHttpClient();

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        com.squareup.okhttp.Response response = null;

        try {
            response = client.newCall(request).execute();
            strReturn = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i("RETURN: ", strReturn);
        return strReturn;
    }

}
