package m.smarthomev2.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.Pair;

import com.example.anders.myapplication.backend.myApi.MyApi;

import m.smarthomev2.MainActivity;
import m.smarthomev2.utils.OKHttp;
import m.smarthomev2.utils.Parser;
//import m.smarthomev2.utils.OKHttp;

public class AmazonAuthenticateTask extends AsyncTask<Pair<Context, String>, Void, String> {

    private static MyApi myApiService = null;
    private final MainActivity main;
    private final String pass;
    final String url;
    String user;
    //private final Context context;
    //private long picId;

    public AmazonAuthenticateTask(MainActivity m, String user, String pass, String url){
        main = m;
        this.pass = pass;
        this.url = url;
        this.user = user;
    }
    
    @Override
    protected String doInBackground(Pair<Context, String>... params) {
        OKHttp a = new OKHttp();
        //HttpMet a = new HttpMet();

        String tempS = null;
        try {
            tempS = a.authenticate(user, pass, url);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Parser.amazonParse(tempS);
    }

    @Override
    protected void onPostExecute(String result) {

        main.summary += "<html><body><img src=\"";
        main.summary += result;
        main.summary += "\"></body></html>";
        main.webview.loadData(main.summary, "text/html", null);

    }
}
