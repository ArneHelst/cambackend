package m.smarthomev2.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.util.Log;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import m.smarthomev2.MainActivity;
import m.smarthomev2.utils.InsecureClient;
import m.smarthomev2.utils.Parser;

/**
 * Created by Anders on 2016-03-01.
 */
public class AsyncOpen extends AsyncTask<Pair<Context, String>, Void, String>{
    private final MainActivity main;
    private final String pass;
    private final String user;

    public AsyncOpen(MainActivity m, String user, String pass) {
        main = m;
        this.pass = pass;
        this.user = user;
    }

    enum Switch{
        ON, OFF;
    }

    String openAddress;

    @Override
    protected String doInBackground(Pair<Context, String>... params) {

        try {
            openAddress = authenticate(user, pass);
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed";
        }
        System.out.println("Background: \n " + openAddress );
        openAddress = Parser.amazonParse(openAddress);
        System.out.println("Parsed B: \n" + openAddress);
        String on = openAddress + "//device/1/set?cmd=ON";
        try {
            switchPower(on);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String off = openAddress + "//device/1/set?cmd=OFF";

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            switchPower(off);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "success";
    }

    public String authenticate(String user, String pass) throws IOException, JSONException {
        String url =  "https://server-cam-env.eu-central-1.elasticbeanstalk.com/server-cam/api/authenticate_door";
        final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        JSONObject json= new JSONObject();
        String strReturn = "";
        try {
            json.put("pass",pass);
            json.put("user", user);
        } catch (JSONException e) {
            System.out.println("Error JSON");
            e.printStackTrace();
        }
        OkHttpClient client = InsecureClient.getUnsafeOkHttpClient();
        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        com.squareup.okhttp.Response response = null;
        try {
            response = client.newCall(request).execute();
            strReturn = response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.i("RETURN: ", strReturn);
        return strReturn;

    }

    private final String USER_AGENT = "Mozilla/5.0";

    private String switchPower(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        int responseCode = con.getResponseCode();

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        String urlParameters = "";
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }
}
