package m.smarthomev2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;

import m.smarthomev2.tasks.AmazonAuthenticateTask;
import m.smarthomev2.tasks.AsyncOpen;

public class MainActivity extends AppCompatActivity {
    public WebView webview;
    public String summary;
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final String amazonServerURL = "https://server-cam-env.eu-central-1.elasticbeanstalk.com/server-cam/api/authenticate";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webview = (WebView) findViewById(R.id.webView);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //final EditText editText = (EditText) findViewById(R.id.editPass);
        setSupportActionBar(toolbar);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = prefs.getString("username", "");
                String pass = prefs.getString("password","");
                new AmazonAuthenticateTask(MainActivity.this, user, pass, amazonServerURL).execute();
            }
        });

        FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = prefs.getString("username", "");
                String pass = prefs.getString("password","");
                System.out.println("user: " + user);
                System.out.println("pass: " + pass);
                new AsyncOpen(MainActivity.this, user, pass).execute();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }



        return super.onOptionsItemSelected(item);
    }

}
