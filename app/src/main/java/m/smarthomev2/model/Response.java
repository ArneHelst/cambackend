package m.smarthomev2.model;

import java.io.Serializable;

/**
 * Created by Anders on 2016-02-12.
 */
public class Response implements Serializable{
    private String myData;

    public String getData() {
        return myData;
    }

    public void setData(String data) {
        myData = data;
    }


}
